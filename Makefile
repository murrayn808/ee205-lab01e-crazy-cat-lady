###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01e - CrazyCatLady - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build a user input print statement using C++
###
### @author  @Nathaniel Murray <@murrayn@hawaii.edu>
### @date    @1/12/2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = hello

all: $(TARGET)

/bin/bash: line 1: :q: command not found
